using UnityEngine;
using System.Collections;

public class GUIControl : MonoBehaviour {
	NightVisionEffect nightVissionEffect = null;
	GUIStyle style;
	
	// Use this for initialization
	void Awake () {
		nightVissionEffect = GameObject.FindObjectOfType(typeof(NightVisionEffect)) as NightVisionEffect;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	bool isEnable = true;
	float barSize = 1f;
	void OnGUI(){
		if(nightVissionEffect==null){
			return;
		}
		
		if(GUILayout.Button("Switch",GUILayout.Width(100),GUILayout.Height(30))){
			isEnable = !isEnable;
			nightVissionEffect.enabled = isEnable;
		}
		
		GUILayout.Label("press w,s,a,d to rotate camera");
		
		if(isEnable){
			nightVissionEffect.hasVignetteTexture =  GUILayout.Toggle(nightVissionEffect.hasVignetteTexture,"Vignette");
			nightVissionEffect.hasScanLineTexture =  GUILayout.Toggle(nightVissionEffect.hasScanLineTexture,"Scan Line");
			nightVissionEffect.hasNightVisionNoise = GUILayout.Toggle(nightVissionEffect.hasNightVisionNoise,"Screen Noise");
			
			
			GUILayout.Label("contrast");
			nightVissionEffect.contrast = GUILayout.HorizontalScrollbar(nightVissionEffect.contrast,barSize,nightVissionEffect.contrastRange.x,nightVissionEffect.contrastRange.y+barSize,GUILayout.Width(300));
			GUILayout.Label("brightness");
			nightVissionEffect.brightness = GUILayout.HorizontalScrollbar(nightVissionEffect.brightness,barSize,nightVissionEffect.brightnessRange.x,nightVissionEffect.brightnessRange.y+barSize,GUILayout.Width(300));
			GUILayout.Label("distortion");
			nightVissionEffect.distortion = GUILayout.HorizontalScrollbar(nightVissionEffect.distortion,barSize,nightVissionEffect.distortionRange.x,nightVissionEffect.distortionRange.y+barSize,GUILayout.Width(300));
			GUILayout.Label("scale");
			nightVissionEffect.scale = GUILayout.HorizontalScrollbar(nightVissionEffect.scale,barSize,nightVissionEffect.scaleRange.x,nightVissionEffect.scaleRange.y+barSize,GUILayout.Width(300));
			
			
		}
		
		 //GUILayout.HorizontalScrollbar()
	}
}
