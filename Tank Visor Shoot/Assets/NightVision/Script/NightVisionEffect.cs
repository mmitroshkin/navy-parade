using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class NightVisionEffect : MonoBehaviour {
	
	#region  Variables
	public Shader nightVisionShader;
	
	public float contrast = 2f;
	public float brightness = 0.1f;
	public Color nightVisionColor = Color.green;
	
	public bool hasVignetteTexture = true;
	public Texture2D vignetteTexture;
	
	
	public bool hasScanLineTexture = true;
	public Texture2D scanLineTexture;
	public float scanLineTileAmount = 4f;
	
	
	public bool hasNightVisionNoise = true;
	public Texture2D nightVisionNoise;
	public float noiseXSpeed = 100f;
	public float noiseYSpeed = 100f;
	
	public float distortion = 0.2f;
	public float scale = 1f;
	
	private float randomValue = 0f;
	private Material curMaterial;
	
	Material material{
		get{
			if(curMaterial == null){
				curMaterial = new Material(nightVisionShader);
				curMaterial.hideFlags = HideFlags.HideAndDontSave;
			}
			return  curMaterial;
		}
	}
	#endregion
	
	// Use this for initialization
	void Start () {
		//if(!SystemInfo.supportsImageEffects){
		//	enabled = false;
		//	return;
		//}
		
//		nightVisionShader = Shader.Find("NightVision/NightVisionEffectShader");
		
		if(!nightVisionShader && !nightVisionShader.isSupported)
		{
			enabled = false;
		}
		
		
		
	}
	
	[HideInInspector]
	public Vector2 contrastRange = new Vector2(0,4f);
	[HideInInspector]
	public Vector2 brightnessRange = new Vector2(0,1f);
	[HideInInspector]
	public Vector2 distortionRange = new Vector2(-1,1f);
	[HideInInspector]
	public Vector2 scaleRange = new Vector2(0,1f);
	// Update is called once per frame
	void Update () {
		contrast = Mathf.Clamp(contrast,contrastRange.x,contrastRange.y);
		brightness = Mathf.Clamp(brightness,brightnessRange.x, brightnessRange.y);
		randomValue = Random.Range(-1, 1);
		distortion = Mathf.Clamp(distortion,distortionRange.x, distortionRange.y);
		scale = Mathf.Clamp(scale,scaleRange.x, scaleRange.y);
	}
	
	void OnRenderImage(RenderTexture sourceTexture, RenderTexture destTexture){
		if(nightVisionShader !=null){
			material.SetFloat("_Contrast",contrast);
			material.SetFloat("_Brightness",brightness);
			material.SetColor("_NightVisionColor",nightVisionColor);
			material.SetFloat("_RandomValue",randomValue);
			material.SetFloat("_distortion",distortion);
			material.SetFloat("_scale",scale);
			
			if(vignetteTexture&&hasVignetteTexture){
				material.SetFloat("_hasVignetteTex",1);
				material.SetTexture("_VignetteTex",vignetteTexture);
			}else{
				material.SetFloat("_hasVignetteTex",0);
			}
			
			if(scanLineTexture&&hasScanLineTexture){
				material.SetFloat("_hasScanLineTex",1);
				material.SetTexture("_ScanLineTex",scanLineTexture);
				material.SetFloat("_ScanLineTileAmount",scanLineTileAmount);
			}else{
				material.SetFloat("_hasScanLineTex",0);
			}
			
			if(nightVisionNoise&&hasNightVisionNoise){
				material.SetFloat("_hasNoiseTex",1);
				material.SetTexture("_NoiseTex",nightVisionNoise);
				material.SetFloat("_NoiseXSpeed",noiseXSpeed);
				material.SetFloat("_NoiseYSpeed",noiseYSpeed);
			}else{
				material.SetFloat("_hasNoiseTex",0);
			}
			
//			Graphics.Blit(sourceTexture,destTexture);
			Graphics.Blit(sourceTexture,destTexture,material);
		}else{
			Graphics.Blit(sourceTexture,destTexture);
		}
	}
		
}
