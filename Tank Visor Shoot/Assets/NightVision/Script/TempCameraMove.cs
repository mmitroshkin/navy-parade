using UnityEngine;
using System.Collections;

public class TempCameraMove : MonoBehaviour {
	public float speed = 200;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
//		transform.Translate(Vector3.forward*speed*Time.deltaTime,Space.Self);
		if(Input.GetKey(KeyCode.W)){
			transform.Rotate(Vector3.left*30*Time.deltaTime,Space.Self);
		}
		
		if(Input.GetKey(KeyCode.S)){
			transform.Rotate(Vector3.right*30*Time.deltaTime,Space.Self);
		}
		
		if(Input.GetKey(KeyCode.A)){
			transform.Rotate(Vector3.down*30*Time.deltaTime,Space.World);
		}
		
		if(Input.GetKey(KeyCode.D)){
			transform.Rotate(Vector3.up*30*Time.deltaTime,Space.World);
		}
		
	}
}
