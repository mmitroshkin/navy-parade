using UnityEngine;
using System.Collections;
using UnityEditor;

public class AddNightVisionEffect : Editor {
	[MenuItem ("Component/Night Vision/Night Vision Effect")]
	static void AddToCamera(){
		Transform selectedTran = Selection.activeTransform;
		if(selectedTran == null|| selectedTran.GetComponent<Camera>() == null){
			Debug.LogError("Please choose a Camera in scene!");
			return;
		}
		
		if(selectedTran.GetComponent<NightVisionEffect>()!=null){
//			Debug.LogError("Already added.Cannot add anymore!");
			NightVisionEffect nightVisionEffectTemp =  selectedTran.GetComponent<NightVisionEffect>();
			DestroyImmediate(nightVisionEffectTemp);
		}
		
		
		NightVisionEffect nightVisionEffect = selectedTran.gameObject.AddComponent<NightVisionEffect>();
		nightVisionEffect.nightVisionShader = Shader.Find("NightVision/NightVisionEffectShader");
		
		nightVisionEffect.nightVisionNoise = Resources.Load("Textures/NoiseTex",typeof(Texture2D)) as Texture2D;
		nightVisionEffect.vignetteTexture = Resources.Load("Textures/VignetteTex",typeof(Texture2D)) as Texture2D;
		nightVisionEffect.scanLineTexture = Resources.Load("Textures/Line",typeof(Texture2D)) as Texture2D;
		
		Debug.Log("Add Night Vision Effect to Camera");
		
	}
	
	
}
