﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IzTarget : MonoBehaviour
{
	public enum Type
	{
		none,
		human,
		tank,
		helicopter
	}


	public	Type	type			=	Type.none;

	[Range(0f, 1f)]
	public	float	termo			=	1;
	public	float	termoSpeedIn	=	10f;
	public	float	termoSpeedOut	=	30f;

	public	float	health			=	100;
	public	float	speed			=	5f;
	public	float	speedR			=	20f;
	public	float	distance		=	300f;

	public	bool	move			=	false;

	public	Vector3	positionTo		=	Vector3.zero;

	public MeshRenderer[]		meshRenderers;
	public SpriteRenderer[]		spriteRenderers;

	bool		alive_				=	true;
	Animator	animator_;

	float		termo_				=	1;
	Material	material_;

	Color		color_				=	new Color(1, 1, 1);
	Color		colorMax_			=	new Color(1f, 1f, 1f);
	Color		colorMin_			=	new Color(60f/256f, 60f/256f, 60f/256f);

	Vector3		helDir_				=	Vector3.zero;

	Collider	collider_;

	public void Kill ()
	{
		alive_		=	false;

		if (type == Type.human)
		{
			animator_.SetBool("kill", true);

			IzRoot.KillTarget(this);
		}

		if (type == Type.tank)
		{
			animator_.SetBool("explode", true);
			IzRoot.CreateExplosionGroundBig(transform.position);

			IzRoot.KillTarget(this);
		}

		if (type == Type.helicopter)
		{
			var	pos			=	transform.position;
				pos.y		=	0;

			var	loorRot		=	Quaternion.LookRotation(pos);
			var	angle		=	loorRot.y + Random.Range(-90f, 90f);

			helDir_			=	new Vector3(Mathf.Sin(angle * Mathf.Deg2Rad), 0, Mathf.Cos(angle * Mathf.Deg2Rad));
		}
	}

	public void Spawn (float _y = 0f)
	{
		var	angleBack		=	IzRoot.angleTo - 180f;
		var	angleSpawn		=	angleBack + Random.Range(-90f, 90f);
		
		var	dist			=	distance * 0.9f;

		var	posFrom			=	new Vector3(Mathf.Sin(angleSpawn * Mathf.Deg2Rad), 0, Mathf.Cos(angleSpawn * Mathf.Deg2Rad)) * dist;
		
		var	up				=	_y > 0 ? (Vector3.up * _y) : Vector3.zero;

		transform.position	=	posFrom + up;

		SetPositionTo();

		transform.rotation	=	Quaternion.LookRotation(transform.position, positionTo);

		termo				=	0;
	}

	void SetPositionTo ()
	{
		var	angle			=	Quaternion.LookRotation(transform.position).eulerAngles.y;
		var	angleN			=	angle  + 90f * (Random.Range(0f, 1f) >= 0.5f ? 1f : -1f);

		var	posToMax		=	new Vector3(Mathf.Sin(angleN * Mathf.Deg2Rad), 0, Mathf.Cos(angleN * Mathf.Deg2Rad)) * distance; 

		var	posTo			=	Vector3.Lerp(Vector3.zero, posToMax, Random.Range(0.2f, 0.7f));

		var	up				=	transform.position.y > 0 ? (Vector3.up * transform.position.y) : Vector3.zero;
		
		positionTo			=	GetPositionTo(transform.position, posTo) + up;
	}

	Vector3 GetPositionTo (Vector3 _fprom, Vector3 _to)
	{
		_fprom.y		=	0;
		_to.y			=	0;

		var	direction	=	(_to - _fprom).normalized;
		var pointTo		=	_to;
		var	dist		=	pointTo.magnitude;
		while (dist < distance)
		{
			pointTo		+=	direction;
			dist		=	pointTo.magnitude;
		}

		return pointTo;
	}

    // Start is called before the first frame update
    void Start()
    {
		collider_	=	GetComponent<BoxCollider>();
		if (type == Type.human)
			collider_.enabled	=	false;

        animator_	=	GetComponent<Animator>();

		if (meshRenderers.Length > 0)
		{
			material_	=	new Material(meshRenderers[0].material);

			var	color	=	Color.Lerp(colorMin_, colorMax_, termo);

			material_.SetColor("_Color", color);

			for (var i=0; i<meshRenderers.Length; i++)
				meshRenderers[i].material	=	material_;
		}

		SetPositionTo();
    }

    // Update is called once per frame
    void Update()
    {
		//if (type == Type.human && animator_.)


		if (alive_ && health <= 0) {
			alive_	=	false;
			Kill();
		}

		if (!alive_)
			UpdateKill();
		else
		{
			UpdatePosition();
		}
		
		UpdateTermo();
    }

	void UpdatePosition()
    {

		if (health <= 0)
			return;

		var	direction			=	positionTo - transform.position;
		var	rotationTo			=	Quaternion.LookRotation(direction);
		var	distance			=	direction.magnitude;

		if (distance > 10f)
		{
			//	если надо повернуть
			if (Quaternion.Angle(transform.rotation, rotationTo) > 1f)
				transform.rotation	=	Quaternion.RotateTowards(transform.rotation, rotationTo, Time.deltaTime * speedR);
			else
			{
				if (health > 0)
					transform.position	+=	transform.forward * (Time.deltaTime * speed);
			}
		}
		else
			SetPositionTo();
	}

	void UpdateKill()
    {
		if (type == Type.helicopter)
		{
			if (transform.position.y > 0 )
			{
				transform.Rotate(Vector3.up * (Time.deltaTime * 180));

				transform.Rotate(Vector3.left * (Time.deltaTime * 60));

				transform.position	+=	helDir_ * (Time.deltaTime * 10);
				transform.position	+=	Vector3.down * (Time.deltaTime * 10);
			}
			else
			{
				IzRoot.KillTarget(this);

				Destroy(gameObject);
			}

		}
	}

	void UpdateTermo()
    {
		//	нагревание после появления
		if (health > 0 && termo < 1)
		{
			termo	+=	Time.deltaTime / termoSpeedIn;
			if (termo > 1)
				termo = 1;
		}

		//	остывание после смерти
		if (health <= 0 && termo > 0)
		{
			termo	-=	Time.deltaTime / termoSpeedOut;
			if (termo < 0)
				termo = 0;
		}

		//	обновление материалов
		if (termo_ != termo) {
			termo_		=	termo;

			color_		=	Color.Lerp(colorMin_, colorMax_, termo);

			if (meshRenderers.Length > 0)
			{
				material_.SetColor("_Color", color_);

				for (var i=0; i<meshRenderers.Length; i++)
				{
					if (Application.isPlaying)
						meshRenderers[i].material		=	material_;
					else
						meshRenderers[i].sharedMaterial	=	material_;
				}
			}

			if (spriteRenderers.Length > 0)
			{
				for (var i=0; i<spriteRenderers.Length; i++)
					spriteRenderers[i].color	=	color_;
			}
			
		}

		//	удаление уничтоженного объекта если он позади камеры
		if (health <= 0 && termo_ <= 0 && IzRoot.IsBackCamera(transform.position))
			Destroy(gameObject);
	}

	//	end
}
