﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IzRezult : MonoBehaviour
{
	

	static	public	int		mens	=	0;
	static	public	int		tanks	=	0;
	static	public	int		helys	=	0;

	public GameObject	mensContainer;
	public Text			mensText;

	public GameObject	tankContainer;
	public Text			tankText;

	public GameObject	helyContainer;
	public Text			helyText;

	public Text			allText;

	public GameObject	buttonRestart;
	
	float		pos_time_	=	10;
	int			pos_		=	10;

	public AudioSource		audio;

	public void ButtonRestart ()
    {
        //mens	=	Random.Range(10, 50);
		//tanks	=	Random.Range(10, 50);
		//helys	=	Random.Range(10, 50);

		IzRoot.SetResult(false);
    }

	public	void OnEnable()
    {
		Show();
	}

	public	void Show()
    {
        gameObject.SetActive(true);

		mensContainer.SetActive(false);
		tankContainer.SetActive(false);
		helyContainer.SetActive(false);
		allText.gameObject.SetActive(false);

		buttonRestart.SetActive(false);

		pos_time_	=	0;
		pos_		=	0;
		audio.Play();
    }


    // Start is called before the first frame update
    void Start()
    {

		//gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
		if (!gameObject.activeSelf || pos_ >= 5)
			return;

		pos_time_	+=	Time.deltaTime;

		var	pos	=	Mathf.CeilToInt(pos_time_);

		if (pos_ != pos) {
			pos_  = pos;

			mensContainer.SetActive(pos_ >= 1);
			tankContainer.SetActive(pos_ >= 2);
			helyContainer.SetActive(pos_ >= 3);

			allText.gameObject.SetActive(pos_ >= 4);

			if (pos_ > 1)
				IzRoot.CreateSoundShootBig();
		}

		var	percent		=	1f - (pos_ - pos_time_);
		if (percent > 1f)
			percent = 1f;

		var	price_m		=	50;
		var	price_t		=	100;
		var	price_h		=	300;


		var	mensItog	=	price_m * mens;
		var	tanksItog	=	price_t * tanks;
		var	helysItog	=	price_h * helys;
		var	itog		=	mensItog + tanksItog + helysItog;

		mensText.text	=	price_m + " x "  + mens + " = " + (pos_time_ >= 1 ? mensItog : (int)(mensItog * percent));
		tankText.text	=	price_t + " x " + tanks + " = " + (pos_time_ >= 2 ? tanksItog : (int)(tanksItog * percent));
		helyText.text	=	price_h + " x " + helys + " = " + (pos_time_ >= 3 ? helysItog : (int)(helysItog * percent));
		allText.text	=	"итого: " + (pos_time_ >= 4 ? itog : (int)(itog * percent));

		if (pos_ >= 5)
		{
			audio.Stop();
			buttonRestart.SetActive(true);
		}

        //if (if)
    }



}
