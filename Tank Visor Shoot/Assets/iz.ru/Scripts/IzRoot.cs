﻿using UnityEngine;
using UnityEngine.UI;

public class IzRoot : MonoBehaviour
{
	static public float angleTo	=	0f;

	static IzRoot _;

	static bool isResult_		=	false;

	static	float	timerMax	=	3f * 60f;
	static	float	timer		=	0;

	static	int		bulletsBigCount			=	0;//30;
	static	int		bulletsSmallCount		=	0;//1000;

	static	int		bulletsBigCountMax		=	30;
	static	int		bulletsSmallCountMax	=	1000;
	

	static public void SetResult (bool _value)
	{
		isResult_	=	_value;

		_.windowResult.SetActive(_value);
		_.musicGame.SetActive(!_value);
		_.musicResult.SetActive(_value);

		if (!_value)
			SetReset();
	}

	static void SetReset()
    {
		IzRezult.mens	=	0;
		IzRezult.tanks	=	0;
		IzRezult.helys	=	0;

		timer				=	timerMax;
		bulletsBigCount		=	bulletsBigCountMax;
		bulletsSmallCount	=	bulletsSmallCountMax;
	}


	static public void CreateSoundShootBig ()
	{
		Instantiate(_.prefabs.bulletBigAudio);
	}

	static public Vector3 GetRandomSpawnFrom()
	{
		var	angle		=	angleTo - 180f + Random.Range(-90f, 90f);

		var	angleN		=	angle  + 90f * (Random.Range(0f, 1f) >= 0.5f ? 1f : -1f);


		return Vector3.zero;
	}

	static public GameObject CreateExplosionGroundBig (Vector3 _position)
	{
		_position.y	=	-2;

		Instantiate(_.prefabs.crater, _position, Quaternion.identity);

		return	Instantiate(_.prefabs.explosionBigGroundBig,	_position, Quaternion.identity);
	}

	static public void CreateTargetSmall ()
	{
		var	angle		=	Random.Range(0f, 360f);
		var	direction	=	new Vector3(Mathf.Sin(angle * Mathf.Deg2Rad), 0, Mathf.Cos(angle * Mathf.Deg2Rad));

		var human	=	Instantiate(_.prefabs.targetSmall);
			human.transform.position	=	direction * Random.Range(40f, 120f);
			human.transform.rotation	=	Quaternion.LookRotation(-human.transform.position);
	}

	static public void CreateTargetTank ()
	{
		var tank	=	Instantiate(_.prefabs.targetTank);
			tank.GetComponent<IzTarget>().Spawn();
			tank.transform.rotation	=	Quaternion.LookRotation(tank.transform.position);
	}

	static public IzTarget CreateHelicopter ()
	{	
		var	helicopter	=	Instantiate(_.prefabs.targetHelicopter).GetComponent<IzTarget>();
			helicopter.Spawn(40);

		return helicopter;
	}

	static public void HitTarget (IzBullet _bullet, IzTarget _target)
	{
		if (_bullet.type == IzBullet.Type.big)
		{
			if (_target != null && _target.type == IzTarget.Type.helicopter)
				_.ExplosionAir(_bullet.transform.position);
			else
				_.ExplosionGround(_bullet.transform.position);
		}

		
	}

	static public void HitGround (IzBullet _bullet)
	{
		if (_bullet.type == IzBullet.Type.big)
		{
			_.ExplosionGround(_bullet.transform.position);
		}
	}


	static public void KillTarget (IzTarget _target)
	{
		//	спавню малую цель
		if (_target.type == IzTarget.Type.human)
		{
			IzRezult.mens++;
			CreateTargetSmall();
		}

		if (_target.type == IzTarget.Type.tank)
		{
			IzRezult.tanks++;
			CreateTargetTank();
		}
			
		if (_target.type == IzTarget.Type.helicopter)
		{
			IzRezult.helys++;

			var explosion	=	CreateExplosionGroundBig(_target.transform.position);
				explosion.transform.localScale	=	Vector3.one * 10f;

			CreateHelicopter();
		}
	}

	static public bool IsBackCamera (Vector3 _position)
	{
		return Vector3.Angle(Camera.main.transform.forward, _position) > 90;
	}


	[System.Serializable]
	public class Prefabs
	{
		public GameObject	bulletSmall;
		public GameObject	bulletBig;
		public GameObject	bulletBigAudio;

		public GameObject	explosionBigGround;
		public GameObject	explosionBigGroundBig;
		public GameObject	explosionAir;

		public GameObject	crater;

		public GameObject	targetTank;
		public GameObject	targetHelicopter;
		public GameObject	targetSmall;
		
	}


	public	Transform[]			redBall;


	public	NightVisionEffect	nightVisionEffect;

	public	Transform			muzzleSmall;
	public	Transform			muzzleBig;

	public	AudioSource			audioSmall;

	public	Text				textMetrs;
	public	Text				textBulletsSmall;
	public	Text				textBulletsBig;
	public	Text				textTimer;

	public	Prefabs				prefabs;

	public bool	spawnTank		=	false;

	public	GameObject			help;
	public	GameObject			toHor;

	public	GameObject			musicGame;
	public	GameObject			musicResult;
	public	GameObject			windowResult;

	bool	zoomIn_				=	false;
	bool	zoomOut_			=	false;

	float	zoomMim_			=	1f;
	float	zoomMax_			=	0.4f;
	float	zoomSpeed_			=	0.5f;

	bool	shootSmallPress_	=	false;
	float	shootSmallTime_		=	0;
	float	shootSmallTimeD_	=	0;

	bool	shootSmall_			=	false;

	

	float	shootBigTime_		=	0;

	Vector3	positionTo_			=	Vector3.zero;

	bool	pressL_				=	false;
	bool	pressR_				=	false;
	bool	pressU_				=	false;
	bool	pressD_				=	false;

	public void ButtonDebug ()
	{
		SetResult(true);
	}


	public void ButtonZoomIn (bool _press)
	{
		zoomIn_			=	_press;
	}

	public void ButtonZoomOut (bool _press)
	{
		zoomOut_		=	_press;
	}

	//	
	public void ButtonShootSmall (bool _press)
	{
		shootSmallPress_	=	_press;

		if (!_press)
			shootSmallTimeD_	=	0.2f;
	}

	public void ButtonShootBig ()
	{
		if (shootBigTime_ <= 0 && bulletsBigCount > 0) {
			shootBigTime_	=	3;
			bulletsBigCount--;
			ShootBig();
		}
		
	}

	public void ButtonLeft (bool _press)
	{
		pressL_	=	_press;
	}

	public void ButtonRight (bool _press)
	{
		pressR_	=	_press;
	}

	public void ButtonUp (bool _press)
	{
		pressU_	=	_press;
	}

	public void ButtonDown (bool _press)
	{
		pressD_	=	_press;
	}


	void ShootSmall ()
    {
		var positionCenter		=	(positionTo_ - muzzleSmall.position) / 2f;
		var positionTo			=	positionCenter + muzzleSmall.up * (positionTo_.magnitude / 100);

        var bullet	=	Instantiate(prefabs.bulletSmall, muzzleSmall.position, muzzleSmall.rotation).GetComponent<Rigidbody>();
			bullet.AddForce(positionTo.normalized * 4000);//Random.Range(3000, 4000));
    }

	void ShootBig ()
    {
		var	direction			=	(positionTo_ + Vector3.up * (positionTo_.magnitude / 100) - muzzleBig.position).normalized;
		var	rotation			=	Quaternion.LookRotation(direction);

        var bullet	=	Instantiate(prefabs.bulletBig, muzzleBig.position, rotation).GetComponent<Rigidbody>();
			bullet.AddForce(direction * 4000);

		Instantiate(prefabs.bulletBigAudio);
    }

	public void ExplosionGround (Vector3 _position)
	{
		_position.y	=	0;

		Instantiate(prefabs.explosionBigGround, _position, Quaternion.identity);
		Instantiate(prefabs.crater,				_position, Quaternion.identity);
	}

	public void ExplosionAir (Vector3 _position)
	{
		Instantiate(prefabs.explosionAir, _position, Quaternion.identity);
	}

	void OnEnable () { _ = this; }

	void OnDisable () { _ = null; }

    //
    void Start()
    {
        CreateTargetSmall();
		CreateTargetSmall();
		CreateTargetSmall();
		CreateTargetSmall();
		CreateTargetSmall();

		help.SetActive(true);

		SetReset();
    }

	public static Vector3 GetPointOnPlane () 
	{
		var	screenCenter	=	new Vector2 (Screen.width / 2, Screen.height / 2);
		var	ray 			= 	Camera.main.ScreenPointToRay(screenCenter);	//	Input.mousePosition
		var plane			=	new Plane(Vector3.up, Vector3.zero);
		var	hitPoint		=	Vector3.zero;
		var	hitDist			=	0f;
		
		if (plane.Raycast(ray, out hitDist))
			hitPoint	= 	ray.GetPoint(hitDist);

		return new Vector3(hitPoint.x, hitPoint.y, hitPoint.z);
	}


	public static Vector3 GetPointHit () 
	{
		var	screenCenter	=	new Vector2 (Screen.width / 2, Screen.height / 2);
		var	ray 			= 	Camera.main.ScreenPointToRay(screenCenter);
		var	hitPoint		=	Vector3.zero;

		RaycastHit hit;

		if (Physics.Raycast(ray, out hit)) {
            Transform objectHit = hit.transform;

			hitPoint		=	hit.point;
            
            // Do something with the object that was hit by the raycast.
        }

		return hitPoint;
	}


	/*
	public static Vector3 GetPointOnPlane (Vector3 _from, Vector3 _to) 
	{
		var	ray 			= 	Camera.main.ScreenPointToRay(_point);
		var plane			=	new Plane(Vector3.up, Vector3.zero);
		var	hitPoint		=	Vector3.zero;
		var	hitDist			=	0f;
		
		if (plane.Raycast(ray, out hitDist))
			hitPoint	= 	ray.GetPoint(hitDist);

		return new Vector3(hitPoint.x, hitPoint.y, hitPoint.z);
	}
	*/

	void SpawnTank ()
    {
		var tank		=	Instantiate (prefabs.targetTank).GetComponent<IzTarget>();
			tank.Spawn();
	}


    //
    void Update()
    {
		

		if (isResult_)
		{
			if (nightVisionEffect.scale < zoomMim_)
				nightVisionEffect.scale +=	Time.deltaTime * zoomSpeed_;

			transform.Rotate(Vector3.up	* 5f * Time.deltaTime, Space.Self);

			return;
		}
		
		if (timer > 0 && !help.activeSelf)
			timer	-=	Time.deltaTime;
		if (timer < 0)
			timer	=	0;

		

		//	хелп в начале
		if (help.activeSelf && Input.anyKeyDown)
			help.SetActive(false);

		//	повернуть на горизонталь
		toHor.SetActive(Screen.width <= Screen.height);

		if (zoomIn_ || zoomOut_ && zoomIn_ != zoomOut_ )
			nightVisionEffect.scale		+=	(zoomOut_ ? 1f : -1f) * (Time.deltaTime * zoomSpeed_);

		if (nightVisionEffect.scale > zoomMim_)
			nightVisionEffect.scale = zoomMim_;
		if (nightVisionEffect.scale < zoomMax_)
			nightVisionEffect.scale = zoomMax_;

		//	------------------------------------------

		var zoomPercent		=	(nightVisionEffect.scale - zoomMax_) / (zoomMim_ - zoomMax_);
		var	pullHor			=	10 + 20 * zoomPercent;
		var	pullVer			=	10 + 10 * zoomPercent;
		
		if (Input.GetKey(KeyCode.W) || pressU_)
			transform.Rotate(Vector3.left	* pullVer * Time.deltaTime, Space.Self);
		
		if (Input.GetKey(KeyCode.S) || pressD_)
			transform.Rotate(Vector3.right	* pullVer * Time.deltaTime, Space.Self);
		
		if (Input.GetKey(KeyCode.A) || pressL_)
			transform.Rotate(Vector3.down	* pullHor * Time.deltaTime, Space.World);
		
		if (Input.GetKey(KeyCode.D) || pressR_)
			transform.Rotate(Vector3.up		* pullHor * Time.deltaTime, Space.World);

		var angles		=	transform.rotation.eulerAngles;
		if (angles.x < 90 && angles.x > 10)
			angles.x = 10;
		if (angles.x > 270 && angles.x < 320)
			angles.x = 320;

		angleTo			=	angles.y;

		transform.rotation	=	Quaternion.Euler(angles);



        

		//	------------------------------------------

		positionTo_		=	GetPointHit();
		if (positionTo_.Equals(Vector3.zero))
			positionTo_		=	GetPointOnPlane();
		if (positionTo_.Equals(Vector3.zero))
			positionTo_		=	Camera.main.transform.forward * 1000;

		
		textMetrs.text				=	"" + (int)(positionTo_.magnitude * 3);

		textBulletsSmall.text		=	"" + bulletsSmallCount;
		if (bulletsSmallCount >= 100)
			textBulletsSmall.color	=	Color.white;
		if (bulletsSmallCount < 100)
			textBulletsSmall.color	=	Color.yellow;
		if (bulletsSmallCount == 0)
			textBulletsSmall.color	=	Color.red;
		
		textBulletsBig.text			=	"" + bulletsBigCount;
		if (bulletsBigCount >= 10)
			textBulletsBig.color	=	Color.white;
		if (bulletsBigCount < 10)
			textBulletsBig.color	=	Color.yellow;
		if (bulletsBigCount == 0)
			textBulletsBig.color	=	Color.red;


		var	timerMinutes	=	Mathf.FloorToInt(timer / 60f);
		var	timerSecundes	=	Mathf.FloorToInt(timer - 60f * timerMinutes);

		textTimer.text		=	timerMinutes + ":" + timerSecundes;
		if (timer > timerMax / 2f)
			textTimer.color	=	Color.white;
		if (timer < timerMax / 2f)
			textTimer.color	=	Color.yellow;
		if (timer <= 0)
			textTimer.color	=	Color.red;

		redBall[0].position	=	positionTo_;

		//	------------------------------------------

		if (Input.GetKeyDown(KeyCode.Space))
			ButtonShootBig();


		//	------------------------------------------

		shootSmallTime_			+=	Time.deltaTime;

		var	isSmallPress		=	shootSmallPress_ || Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);

		if (!shootSmallPress_)
			shootSmallTimeD_	-=	Time.deltaTime;

		var isSmallDely			=	shootSmallTimeD_ >= 0;
		var	isSmallShoot		=	(isSmallPress || isSmallDely) && bulletsSmallCount > 0;
		
		if (isSmallShoot && shootSmallTime_ > 0.1f)
		{
			shootSmallTime_	=	0f;
			bulletsSmallCount--;
			ShootSmall();
		}

		if (shootSmall_ != isSmallShoot) 
		{
			shootSmall_		=	isSmallShoot;

			if (shootSmall_)
				audioSmall.Play();
			else
				audioSmall.Pause();
		}

		//	----------------------------------------

		shootBigTime_	-=	Time.deltaTime;
		
		
		//	----------------------------------------


		//var	angle		=	angleTo - 180f;

		//var	angleN		=	angle  + 90f * (Random.Range(0f, 1f) >= 0.5f ? 1f : -1f);
		//var	angleN1		=	angle  - 90f;
		//var	angleN2		=	angle  + 90f;

		
		//redBall[1].position			=	new Vector3(Mathf.Sin(angleN1 * Mathf.Deg2Rad), 0, Mathf.Cos(angleN1 * Mathf.Deg2Rad)) * 200f;
		//redBall[2].position			=	new Vector3(Mathf.Sin(angleN2 * Mathf.Deg2Rad), 0, Mathf.Cos(angleN2 * Mathf.Deg2Rad)) * 200f;

		//redBall[0].position			=	new Vector3(Mathf.Sin(angle * Mathf.Deg2Rad), 0, Mathf.Cos(angle * Mathf.Deg2Rad)) * 200f; 

		//Debug.Log("Vector3: " + vector );

		if (spawnTank) {
			spawnTank	=	false;
			SpawnTank();
		}

		//	если кончились патроны или вышел таймер
		if (!isResult_ && (timer <= 0 || (bulletsSmallCount <= 0 && bulletsBigCount <= 0)))
		{
			SetResult(true);
		}
    }

	//	end
}


