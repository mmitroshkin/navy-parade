﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IzBullet : MonoBehaviour
{
	public enum Type
	{
		small,
		big
	}

	public	Type	type			=	Type.small;

	public	float	damage			=	5f;
	public	float	timeDestroy		=	3f;
	public	float	timeGravity		=	30f;

	float	time_					=	0;

	float	timeGravity_			=	0;

	Rigidbody	rigidbody_;

	Vector3		previousPosition_;

    //	
    void Start()
    {
        rigidbody_			=	gameObject.GetComponent<Rigidbody>();
		previousPosition_	=	transform.position;
    }

    //	
    void Update()
    {
        time_	+=	Time.deltaTime;

		if (time_ > timeDestroy)
			Destroy(gameObject);

		if (transform.position.y < 0) 
			OnCollisionGround();

		timeGravity_	+=	Time.deltaTime;
		if (timeGravity_ >= timeGravity)
			rigidbody_.useGravity	=	true;

		
    }

	void FixedUpdate()
    {
		CheckCollision(previousPosition_);

        previousPosition_ = transform.position;
	}


	void CheckCollision (Vector3 prevPos)
    {
        RaycastHit hit;
        Vector3 direction = transform.position - prevPos;
        Ray ray = new Ray(prevPos, direction);
        float dist = Vector3.Distance(transform.position, prevPos);
        if (Physics.Raycast(ray, out hit, dist))
        {
            transform.position	= hit.point;
			var pos				= hit.point;

        }
    }


	void OnCollisionEnter (Collision _collision)
    {
        //Debug.Log("OnCollisionEnter: " + _collision);

		var target			=	_collision.gameObject.GetComponent<IzTarget>();

		var hitTankBySmall	=	type == Type.small && target != null && target.type == IzTarget.Type.tank;

		if (target != null && !hitTankBySmall)
			target.health	-=	damage;

		IzRoot.HitTarget(this, target);

		Destroy(gameObject);
    }

	void OnCollisionGround ()
    {
		IzRoot.HitGround(this);

		Destroy(gameObject);
	}

	//	end
}
