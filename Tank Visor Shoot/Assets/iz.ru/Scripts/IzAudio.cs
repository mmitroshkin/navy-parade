﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof (AudioSource))]
public class IzAudio : MonoBehaviour
{
    public enum Type
	{
		sound,
		music,
		voice
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//	
	static public float	volumeGlobal
	{
		get {return volumeGlobal_;}
		set {volumeGlobal_ = Mathf.Clamp01(value);}
	}

	//	
	static public float	volumeSound
	{
		get {return volumeSound_;}
		set {volumeSound_ =	Mathf.Clamp01(value);}
	}

	//	
	static public float	volumeMusic
	{
		get {return volumeMusic_;}
		set {volumeMusic_ = Mathf.Clamp01(value);}
	}

	static float	volumeGlobal_	=	1f;
	static float	volumeSound_	=	1f;
	static float	volumeMusic_	=	1f;

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public	Type	type			=	Type.sound;
	public	bool	autoremove		=	false;

	[Range(0f, 1f)]
	public float	volume			=	1f;

	AudioSource		audioSource_;

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// 
	public AudioSource audioSource
	{
		get {
			if (audioSource_ == null)
				audioSource_		=	GetComponent<AudioSource>();
			return audioSource_;
		}
	}
	
	// 
	void Update () 
	{
		if (volume < 0f)
			volume = 0f;

		if (volume > 1f)
			volume = 1f;

		var	volumeResult	=	volume * volumeGlobal;

		if (type == Type.sound)
			volumeResult	*=	volumeSound;

		if (type == Type.music)
			volumeResult	*=	volumeMusic;
		
		audioSource.volume		=	volumeResult;
		audioSource.enabled		=	volumeResult > 0;

		if (Application.isPlaying)
		{
			if (autoremove ) {
				var progress			=	Mathf.Clamp01(audioSource.time / audioSource.clip.length);
				if (progress == 1f)
					Destroy(gameObject);
			}
		}
	}

	//	end
}
